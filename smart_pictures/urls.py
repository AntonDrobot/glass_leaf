from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView
from rest_framework import request

from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^my_galleries/', include('my_galleries.urls')),
    url(r'^$', RedirectView.as_view(url='my_galleries/', permanent=True)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)