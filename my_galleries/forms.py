from django import forms


class PhotoForm(forms.Form):
    photo_file = forms.FileField(
        label='Select Photo'
    )