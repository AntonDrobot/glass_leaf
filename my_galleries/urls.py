from django.conf.urls import url, include
from my_galleries.views import list_photos, index_page
from rest_framework import routers
from my_galleries import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'photos', views.PhotoViewSet)
router.register(r'galleries', views.GalleryViewSet)

urlpatterns = [
    url(r'^$', index_page, name='index_page'),
    url(r'^$list_photos', list_photos, name='list_photos'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',
    {'next_page': '/my_galleries/accounts/login'}),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^imageUpload', views.FileUploadView.as_view()),
]

