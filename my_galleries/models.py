from __future__ import unicode_literals
from easy_thumbnails import fields
from django.db import models

class User(models.Model):
    ADMIN = 'ADMIN'
    PICTURE_OWNER = 'PICTURE_OWNER'
    CONTRIBUTOR = 'CONTRIBUTOR'
    USER_TYPE_CHOICES = (
        (ADMIN, 'Admin'),
        (PICTURE_OWNER, 'Picture Owner'),
        (CONTRIBUTOR, 'Contributor'),
    )
    role_type = models.CharField(max_length=30, choices=USER_TYPE_CHOICES, default=PICTURE_OWNER)
    name = models.CharField(max_length=30)
    country = models.CharField(max_length=30)
    website = models.CharField(max_length=100)
    can_create_gallery = models.BooleanField(default=True)
    max_uploaded_photos = models.IntegerField(default=10)
    last_upload_date = models.DateTimeField('Last Upload Date')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=50)


class Photo(models.Model):
    orientation = models.CharField(max_length=20)
    resolution_x = models.IntegerField()
    resolution_y = models.IntegerField()
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    owner = models.ForeignKey(User)
    link = models.CharField(max_length=200)
    upload_date_time = models.DateTimeField('Upload Date Time')
    photo_file = models.FileField(upload_to='photos/')
    thumbnail = fields.ThumbnailerImageField(upload_to='photos/thumb/', null=True)

    def __str__(self):
        return self.photo_file.name


class PhotoCategory(models.Model):
    category = models.ForeignKey(Category)
    photo = models.ForeignKey(Photo)


class Tag(models.Model):
    name = models.CharField(max_length=200)


class PhotoTag(models.Model):
    tag_id = models.ForeignKey(Tag)
    photo_id = models.ForeignKey(Photo)


class Gallery(models.Model):
    SHARED = 'SHARED'
    PRIVATE = 'PRIVATE'
    GALLERY_TYPE_CHOICES = (
        (SHARED, 'Shared'),
        (PRIVATE, 'Private'),
    )
    name = models.CharField(max_length=200)
    gallery_type = models.CharField(max_length=50, choices=GALLERY_TYPE_CHOICES, default=PRIVATE)
    description = models.CharField(max_length=200)
    owner = models.ForeignKey(User)

    def __str__(self):
        return self.name


class PhotoGallery(models.Model):
    gallery_id = models.ForeignKey(Gallery)
    photo_id = models.ForeignKey(Photo)
    photo_order = models.IntegerField(default=0)


class SmartDevice(models.Model):
    device_type = models.CharField(max_length=200)
    orientation = models.CharField(max_length=200)
    owner = models.ForeignKey(User)
    current_photo = models.ForeignKey(Photo)
    current_gallery = models.ForeignKey(Gallery)


class DisplayedGallery(models.Model):
    gallery_id = models.ForeignKey(Gallery)
    device_id = models.ForeignKey(SmartDevice)


class CachedPhoto(models.Model):
    device_id = models.ForeignKey(SmartDevice)
    photo_id = models.ForeignKey(Photo)
    attribute = models.CharField(max_length=200)