from __future__ import unicode_literals

from django.apps import AppConfig


class MyGalleriesConfig(AppConfig):
    name = 'my_galleries'
