from django.contrib import admin
from my_galleries.models import User
from my_galleries.models import Photo
from my_galleries.models import Gallery

admin.site.register(User)
admin.site.register(Photo)
admin.site.register(Gallery)

# Register your models here.
