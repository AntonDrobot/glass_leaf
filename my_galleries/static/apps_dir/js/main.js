$(document).ready(function() {

    $('.js_check,.checkbox').click(function () {
        if ($(this).find('input').is(':checked')) {
            console.log('check');
            $(this).find('input').attr('checked', true);
            $(this).addClass("check_chk");
        } else {
            $(this).find('input').attr('checked', false);
            $(this).removeClass("check_chk");
        }
    });

    $('.link_dropdown').click(function(){
       $(this).next().slideToggle();
        $(this).parents().toggleClass('active');
    });
});
