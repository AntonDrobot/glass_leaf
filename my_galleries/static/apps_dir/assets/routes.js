angular.module('fileUpload')
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    // Now set up the states
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'assets/my_gallery/my_gallery.html',
            controller: 'myGalleryCtrl'
        });


    // the known route, with missing '/' - let's create alias
    $urlRouterProvider.when('', '/');


    //// use the HTML5 History API
    //$locationProvider.html5Mode(true);
});
