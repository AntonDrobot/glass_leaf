angular.module('fileUpload')
    .controller('myGalleryCtrl', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
    $scope.show_upload = '';
    $scope.uploadPic = function (file) {
        file.upload = Upload.upload({
            url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
            data: {username: $scope.username, file: file}
        });
        $scope.show_upload = 'show';
        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
            });
        }, function (response) {
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }
}]);


//app.controller('MyCtrl', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
//
//    $scope.$watch('files', function () {
//        $scope.upload($scope.files);
//    });
//    $scope.$watch('file', function () {
//        if ($scope.file != null) {
//            $scope.files = [$scope.file];
//        }
//    });
//    $scope.log = '';
//    $scope.percent = [];
//    $scope.spinner_show = [];
//
//    $scope.upload = function (files) {
//        console.log(files);
//        if (files && files.length) {
//            for (var i = 0; i < files.length; i++) {
//               var file = $.extend(files[i],{id:i});
//                console.log(file);
//                if (!file.$error) {
//                    Upload.upload({
//                        url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
//                        data: {
//                            username: $scope.username,
//                            file: file
//                        }
//                    }).then(function (resp) {
//                        //$timeout(function () {
//                        //    $scope.log = 'file: ' +
//                        //    resp.config.data.file.name +
//                        //    ', Response: ' + JSON.stringify(resp.data) +
//                        //    '\n' + $scope.log;
//                        //});
//                    }, null, function (evt) {
//                        //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
//                        //$scope.log = 'progress: ' + progressPercentage +'% ' + evt.config.data.file.name + '\n' + $scope.log;
//                        //$scope.log = progressPercentage;
//                        if(evt.type === 'progress'){
//                            $scope.spinner_show[evt.config.data.file.id] = true;
//                        }else{
//                            $scope.percent[evt.config.data.file.id] = 'complete';
//                        }
//
//                    });
//                }
//            }
//        }
//    };
//}]);
