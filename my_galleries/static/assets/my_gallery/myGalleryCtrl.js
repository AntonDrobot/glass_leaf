angular.module('fileUpload')
    .controller('myGalleryCtrl', ['$scope', 'Upload', '$timeout', '$http', function ($scope, Upload, $timeout, $http) {


    $http({
        method: "GET",
        url: "/my_galleries/photos/"
    }).then(function mySucces(response) {
        $scope.myWelcome = response.data;
    }, function myError(response) {
        $scope.myWelcome = response.statusText;
    });



    $scope.show_upload = '';
    $scope.percent = '';
    $scope.spinner_show = false;
    $scope.test = false;
    $scope.upload = function (file) {
        if(!file){return false;}
        $scope.test = true;
        file.upload = Upload.upload({
            url: '/my_galleries/photos/',
            method: 'POST',
            data: {
                "photo_file": file,
                "resolution_x": 12,
                "resolution_y": 12,
                "upload_date_time": "2016-03-31T14:30:05.539406Z",
                "owner": "http://127.0.0.1:8000/my_galleries/users/1/"
            }
        });
        $scope.show_upload = 'show';
        file.upload.then(function (response) {
            console.log(response);
            $scope.myWelcome.push(response.data);
            $timeout(function () {
                $scope.test = false;
                file.result = response.data;
            });
        }, function (response) {
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
        });
    }
}]);


//app.controller('MyCtrl', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
//
//    $scope.$watch('files', function () {
//        $scope.upload($scope.files);
//    });
//    $scope.$watch('file', function () {
//        if ($scope.file != null) {
//            $scope.files = [$scope.file];
//        }
//    });
//    $scope.log = '';
//    $scope.percent = [];
//    $scope.spinner_show = [];
//
//    $scope.upload = function (files) {
//        console.log(files);
//        if (files && files.length) {
//            for (var i = 0; i < files.length; i++) {
//               var file = $.extend(files[i],{id:i});
//                console.log(file);
//                if (!file.$error) {
//                    Upload.upload({
//                        url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
//                        data: {
//                            username: $scope.username,
//                            file: file
//                        }
//                    }).then(function (resp) {
//                        //$timeout(function () {
//                        //    $scope.log = 'file: ' +
//                        //    resp.config.data.file.name +
//                        //    ', Response: ' + JSON.stringify(resp.data) +
//                        //    '\n' + $scope.log;
//                        //});
//                    }, null, function (evt) {
//                        //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
//                        //$scope.log = 'progress: ' + progressPercentage +'% ' + evt.config.data.file.name + '\n' + $scope.log;
//                        //$scope.log = progressPercentage;
//                        if(evt.type === 'progress'){
//                            $scope.spinner_show[evt.config.data.file.id] = true;
//                        }else{
//                            $scope.percent[evt.config.data.file.id] = 'complete';
//                        }
//
//                    });
//                }
//            }
//        }
//    };
//}]);
