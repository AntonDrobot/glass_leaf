# -*- coding: utf-8 -*-
import status as status
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView

from my_galleries.models import Photo
from my_galleries.models import User
from my_galleries.models import Gallery
# from django.contrib.auth.models import User
from my_galleries.forms import PhotoForm

from rest_framework import viewsets
from my_galleries.serializers import UserSerializer, PhotoSerializer, GallerySerializer
from django.conf import settings
from rest_framework import status

from rest_framework.parsers import MultiPartParser, FormParser


@login_required
def list_photos(request):
    # Handle file upload
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            user = User.objects.get(id=request.user.id)
            new_photo = Photo(photo_file=request.FILES['photo_file'],
                              orientation='h', resolution_x=1,
                              resolution_y=1, title='test_title',
                              description='descrip', owner=user,
                              link='test_link', upload_date_time=timezone.now(),
                              )
            new_photo.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('list_photos'))
    else:
        form = PhotoForm()  # A empty, unbound form

    # Load documents for the list page
    photos = Photo.objects.all()

    # Render list page with the photos and the form
    return render(
        request,
        'list.html',
        {'photos': photos, 'form': form}
    )


def index_page(request):
    return render(request, 'my_galleries/index.html')


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PhotoViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer


class GalleryViewSet(viewsets.ModelViewSet):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer

