# from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework.serializers import ModelSerializer
from easy_thumbnails import fields

from my_galleries.models import User, Gallery, Photo
from rest_framework import serializers
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from easy_thumbnails.files import get_thumbnailer



class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'url', 'name', 'country', 'website', 'can_create_gallery',
                  'max_uploaded_photos', 'last_upload_date', 'is_active')


class GallerySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gallery
        fields = ('id', 'url', 'name')


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    thumbnail = serializers.ImageField('get_thumbnail')

    def get_thumbnail(self):
        thumbnail_options = {'size': (100, 100), 'crop': 'scale'}
        thumbnail = get_thumbnailer(Photo.photo_file).get_thumbnail(thumbnail_options).url
        return thumbnail

    class Meta:
        model = Photo
        fields = ['id', 'url', 'photo_file', 'resolution_x', 'resolution_y', 'upload_date_time', 'owner', 'thumbnail']
